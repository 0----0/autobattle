extends Node
class_name Battle;

enum BattleState { PREPARE, ACTIVE }

var state: BattleState = BattleState.PREPARE

var units: Array[Unit] = [];

var unit_stats: Array[UnitStats] = [];
var unit_configs: Array[UnitConfig] = [];

var focus_idx: int = 0

var UnitScene = preload("res://unit.tscn");

# Called when the node enters the scene tree for the first time.
func _ready():
	Signals.unit_spawn.connect(on_unit_spawned)
	Signals.unit_death.connect(on_unit_death)
	Signals.battle_start.connect(on_battle_start)
	Signals.battle_reset.connect(on_battle_reset)
	Signals.emit_battle_added(self)

func on_unit_spawned(unit: Unit):
	register_unit(unit)

func on_unit_death(unit: Unit):
	units.erase(unit)

func on_battle_start():
	state = BattleState.ACTIVE
	unit_stats = []
	unit_configs = []
	for u in units:
		unit_stats.push_back(u.stats)
		unit_configs.push_back(u.config)

func on_battle_reset():
	state = BattleState.PREPARE
	for u in units:
		u.queue_free()
	units = []
	for i in range(unit_stats.size()):
		var u = UnitScene.instantiate()
		u.stats = unit_stats[i]
		u.config = unit_configs[i]
		add_child(u)

func register_unit(unit: Unit):
	units.push_back(unit)
	unit.battle = self

func is_active():
	return state == BattleState.ACTIVE

func _input(event):
	if state != BattleState.PREPARE:
		return
	if units.size() == 0:
		return
	if focus_idx >= units.size():
		focus_idx = 0
	if focus_idx < 0:
		focus_idx = units.size() - 1
	var unit = units[focus_idx]
	if event.is_action_pressed("ui_left"):
		unit.config.position.x -= 1
	if event.is_action_pressed("ui_right"):
		unit.config.position.x += 1
	if event.is_action_pressed("ui_up"):
		unit.config.position.y -= 1
	if event.is_action_pressed("ui_down"):
		unit.config.position.y += 1
	if event.is_action_pressed("ui_focus_prev"):
		focus_idx -= 1
	elif event.is_action_pressed("ui_focus_next"):
		focus_idx += 1
		
		

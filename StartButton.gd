extends Button

enum ButtonAction {
	Start, Reset
}

var button_action: ButtonAction

# Called when the node enters the scene tree for the first time.
func _ready():
	button_action = ButtonAction.Start
	pressed.connect(on_pressed)
	Signals.battle_start.connect(on_battle_start)
	Signals.battle_reset.connect(on_battle_reset)

func _process(_delta):
	match button_action:
		ButtonAction.Start:
			text = "Start"
		ButtonAction.Reset:
			text = "Reset"

func on_battle_start():
	button_action = ButtonAction.Reset

func on_battle_reset():
	button_action = ButtonAction.Start

func on_pressed():
	match button_action:
		ButtonAction.Start:
			Signals.emit_battle_start()
		ButtonAction.Reset:
			Signals.emit_battle_reset()
	

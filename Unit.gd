extends Node2D
class_name Unit

var battle: Battle;

var current_health;
@export var stats: UnitStats
@export var config: UnitConfig

var attack_timer: Timer
var health_bar: HealthBar
var animation_player: AnimationPlayer


var current_target: Unit;
var current_attack_target: Unit;

func find_target():
	if battle == null:
		return
	var units = battle.units;
	var lowest_distance = null
	var closest_unit = null
	for unit in units:
		if unit.stats.faction == stats.faction:
			continue
		var distance = (position - unit.position).length()
		if lowest_distance == null || distance < lowest_distance:
			lowest_distance = distance
			closest_unit = unit
	current_target = closest_unit

func damage(dmg: int):
	current_health -= dmg
	current_health = max(current_health, 0)
	health_bar.set_percent(current_health as float / stats.max_health as float)
	if current_health <= 0:
		Signals.emit_unit_death(self)
		queue_free()

func _ready():
	current_health = stats.max_health
	attack_timer = $AttackTimer
	health_bar = $HealthBar
	animation_player = $AnimationPlayer
	config = config.duplicate(true)
	Signals.connect("battle_added", on_battle_added)
	Signals.emit_unit_spawn(self)
	pass

func on_battle_added(battle: Battle):
	battle.register_unit(self)

func _physics_process(delta):
	if !battle || !battle.is_active():
		waiting_process(delta)
	else:
		battle_process(delta)

func waiting_process(_delta):
	position = Vector2(config.position.x * 128.0, config.position.y * 128.0)

func battle_process(delta: float):
	if is_attacking():
		return
	find_target()
	if current_target == null:
		return
	if (current_target.position - position).length() > stats.attack_range:	
		position += (current_target.position - position).normalized() * delta * stats.speed;
	else:
		attack_target(current_target)


func attack_target(target: Unit):
	current_attack_target = target
	animation_player.play("attack", 1.0, stats.attack_speed)

func is_attacking():
	return animation_player.is_playing()






func attack_trigger():
	if current_attack_target == null:
		return
	#if (current_attack_target.position - position).length() > attack_range:
		#return
	current_attack_target.damage(stats.attack_damage)

extends Node

signal unit_spawn(unit: Unit)
signal unit_death(unit: Unit)
signal battle_added(battle: Battle)
signal battle_start()
signal battle_reset()

func emit_unit_spawn(unit: Unit):
	emit_signal("unit_spawn", unit);

func emit_unit_death(unit: Unit):
	emit_signal("unit_death", unit)

func emit_battle_added(battle: Battle):
	emit_signal("battle_added", battle)

func emit_battle_start():
	emit_signal("battle_start")

func emit_battle_reset():
	emit_signal("battle_reset")

extends Resource
class_name UnitStats

@export var max_health: int = 50;
@export var attack_damage: int = 5;
@export var attack_range: float = 100;
@export var attack_speed: float = 1.0;
@export var speed: float = 2.0;
enum Faction { PLAYER, ENEMY }
@export var faction: Faction = Faction.PLAYER

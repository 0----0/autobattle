extends Node2D
class_name HealthBar

func set_percent(percent: float):
	set_scale(Vector2(percent, 1))
